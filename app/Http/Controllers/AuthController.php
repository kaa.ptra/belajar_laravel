<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register(){
    	return view('register');
    }

    public function welcome2(Request $request){    	
    	$full_name = $request["full_name"];  		
    	return view('welcome2',compact("full_name"));
    }
}
